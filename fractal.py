import pygame, random
import mandelbrot
import interface

#Turning Debug to 1 will open the game in a window rather than full screen
DEBUG = 0

def main():
    pygame.init()
    monitor_info = pygame.display.Info()
    screen_size = width, height = monitor_info.current_w, monitor_info.current_h

    if DEBUG == 1:
        screen = pygame.display.set_mode(screen_size, pygame.SRCALPHA)
    else:
        screen = pygame.display.set_mode(screen_size, (pygame.FULLSCREEN|pygame.SRCALPHA))

    mandelbrot_generator = mandelbrot.Mandelbrot()
    app_interface = interface.Interface()
    blocks_remaining = []
    color_pattern = [(255, 0, 0), (0, 128, 0), (0, 0, 255), (75, 0, 130), (238, 130, 238)]
    block_color = random.choice(color_pattern)

    #The main game events loop
    translateX = 0
    translateY = 0
    scale = 1

    #Setting up the user interface
    menu_quit_button = interface.Button('Quit Fractal', 10, 0, 'menu', pygame, screen)
    app_interface.add_button(menu_quit_button)

    save_screenshot_button = interface.Button('Save Screenshot', 0, 0, 'interface', pygame, screen)
    app_interface.add_button(save_screenshot_button)

    while True:
        pygame.event.pump()

        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    returnedEvent = app_interface.open_menu(pygame, screen, screen_size)
                    if returnedEvent is menu_quit_button:
                        exit()

                    screen.fill((0,0,0))
                    blocks_remaining = []

            #Event handling for any interface buttons
            if event.type == pygame.MOUSEBUTTONDOWN:
                mouse_pressed = pygame.mouse.get_pressed()
                if mouse_pressed[0] == 1:
                    position_clicked = pygame.mouse.get_pos()

                    for interface_button in app_interface.buttons:
                        if interface_button.type == 'interface':
                            if interface_button.check_if_clicked(position_clicked[0], position_clicked[1]) == True:
                                if interface_button is save_screenshot_button:
                                    app_interface.screenshot(screen, screen_size)

        #Begin work on a basic automatic color changed
        blocks_remaining = mandelbrot_generator.block_draw(pygame, screen, screen_size, block_color, translateX, translateY, scale, blocks_remaining)

        #Draw any overlaying interface elements
        save_screenshot_button.draw_button(50, screen_size[1] - 80)

        pygame.time.wait(400)

        if len(blocks_remaining) == 0:
            pygame.time.wait(5000)
            scale = random.randint(1, 3)
            block_color = random.choice(color_pattern)
            translateX = random.randint(-1 * screen_size[0] / 2, screen_size[0] / 4) * scale
            translateY = random.randint(-1 * screen_size[1] / 2, screen_size[1] / 2)

main()