from PIL import Image

class Button:
    def __init__(self, message, x, y, type, pygame, surface):
        self.pygame = pygame
        self.surface = surface
        self.message = message
        self.button_font_color = 222, 222, 222
        self.button_color = 255, 255, 255
        self.x_position = x
        self.y_position = y
        self.x_offset = 0
        self.y_offset = 0
        self.type = type

        #Padding being the pixels on each side of the rendered font
        self.button_text_padding = 10

    def click_action(self):
        font_render = self.font.render(self.message, True, self.button_color)
        overlay_rect = self.pygame.Rect(self.x_position + self.x_offset, self.y_position + self.y_offset, self.width, self.height)
        self.surface.fill(self.button_font_color, overlay_rect)
        self.surface.blit(font_render, (self.x_position + self.x_offset + self.button_text_padding, self.y_position + self.y_offset + self.button_text_padding))

    def set_visible(self, visible):
        self.visible = visible

    def draw_button(self, x_offset = 0, y_offset = 0):
        self.x_offset = x_offset
        self.y_offset = y_offset

        self.font = self.pygame.font.Font(None, 37)
        button_rendered = self.font.render(self.message, True, self.button_font_color)

        #Draw the button itself - width and height determined by above font length
        font_size = self.font.size(self.message)
        self.width = font_size[0] + self.button_text_padding * 2
        self.height = font_size[1] + self.button_text_padding * 2
        button_rect = self.pygame.Rect(self.x_position + x_offset, self.y_position + y_offset, self.width, self.height)
        self.surface.fill(self.button_color, button_rect)
        self.surface.blit(button_rendered, (self.x_position + x_offset + self.button_text_padding, self.y_position + y_offset + self.button_text_padding))

        self.pygame.display.update()

    def check_if_clicked(self, clicked_at_x, clicked_at_y, mouse_up_action = 0):
        actual_x_position = self.x_position + self.x_offset
        actual_y_position = self.y_position + self.y_offset

        if clicked_at_x <= actual_x_position + self.width and clicked_at_x >= actual_x_position:
            if clicked_at_y >= actual_y_position and clicked_at_y <= actual_y_position + self.height:
                #Bail out condition if checking if the mouse up action was still actually in the button
                if mouse_up_action == 1:
                    self.draw_button(self.x_offset, self.y_offset)
                    return True

                #perform any actions ( visual cues ) on when the button is clicked
                self.click_action()
                self.pygame.display.update()

                #So, the mouse button was clicked down, but need to wait until the mouse up action has occurred
                self.pygame.event.wait()
                mouse_pressed = self.pygame.mouse.get_pressed()

                if mouse_pressed[0] == 0:
                    position_clicked = self.pygame.mouse.get_pos()
                    return self.check_if_clicked(position_clicked[0], position_clicked[1], 1)
            else:
                self.draw_button(self.x_offset, self.y_offset)

            return False

        else:
            self.draw_button(self.x_offset, self.y_offset)
            self.pygame.display.update()

        return False

class Interface:
    def __init__(self):
        self.menu_toggle = 0
        self.offset_width = 0
        self.offset_height = 0
        self.buttons = []

    def add_button(self, button):
        self.buttons.append(button)

    def screenshot(self, screen, screen_size, file_name = 'screenshot.jpg'):
        image = Image.new("RGB", screen_size)

        for x in range(0, int(screen_size[0])):
                    for y in range(0, int(screen_size[1])):
                        image.putpixel((x, y), tuple(screen.get_at((x,y))))

        image.save(file_name)

    def check_for_menu_events(self, pygame):
        while(1):
            pygame.event.wait()

            #Check if a button was clicked
            mouse_pressed = pygame.mouse.get_pressed()
            if mouse_pressed[0] == 1:
                position_clicked = pygame.mouse.get_pos()

                for menu_button in self.buttons:
                    if menu_button.type == 'menu':
                        if menu_button.check_if_clicked(position_clicked[0], position_clicked[1]) == True:
                            return menu_button

            if pygame.key.get_focused():
                pressed_key=pygame.key.get_pressed()

                if pressed_key[pygame.K_ESCAPE]:
                    break

    def open_menu(self, pygame, surface, screen_size):
        pygame.font.init()
        monitor_info = pygame.display.Info()
        menu_width = 500
        menu_height = 350
        title_bar_height = 70
        menu_base_color = (222, 222, 222)
        shadow_width = 25

        self.offset_width = monitor_info.current_w / 2 - (menu_width / 2)
        self.offset_height = monitor_info.current_h / 2 - (menu_height / 2)

        #Draw the main back drop covering the whole screen
        menu_backdrop_surface = pygame.Surface((monitor_info.current_w, monitor_info.current_h))
        menu_backdrop_surface.set_alpha(128)
        menu_backdrop_surface.fill((0, 0, 0))
        surface.blit(menu_backdrop_surface, (0,0))

        #Draw a menu window translucent border
        menu_window_border_surface = pygame.Surface((menu_width + shadow_width * 2, menu_height + shadow_width * 2))
        menu_window_border_surface.set_alpha(128)
        menu_window_border_surface.fill((255, 255, 255))
        surface.blit(menu_window_border_surface, (self.offset_width - shadow_width, self.offset_height - shadow_width))

        #Draw the menu
        menu_rect = pygame.Rect(self.offset_width, self.offset_height, menu_width, menu_height)
        surface.fill(menu_base_color, menu_rect)

        #Draw the menu title
        title_bar = pygame.Rect(self.offset_width, self.offset_height, menu_width, title_bar_height)
        surface.fill((255,255,255), title_bar)

        title_font = pygame.font.Font(None, 65)
        title_message = title_font.render('Options', True, menu_base_color)
        surface.blit(title_message, (10 + self.offset_width, 10 + self.offset_height))
        self.offset_height += title_bar_height + 10

        #Draw any buttons attached to the menu
        for menu_button in self.buttons:
            if menu_button.type == 'menu':
                menu_button.draw_button(self.offset_width, self.offset_height)

        pygame.display.update()

        return self.check_for_menu_events(pygame)