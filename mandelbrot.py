import random

class Mandelbrot:
    def __init__(self):
        self.max_iterations = 25
        self.stored_mandelbrot_calculations = []
        random.seed

    def calculate_new_mandelbrot(self, screen_size, Px, Py, translateX, translateY, scaleFactor):
        # x0 = scaled x coordinate of pixel (scaled to lie in the Mandelbrot X scale (-2.5, 1))
        # y0 = scaled y coordinate of pixel (scaled to lie in the Mandelbrot Y scale (-1, 1))

        # The actual calculation is: from (http://stackoverflow.com/a/929107)
        # OldRange = (OldMax - OldMin)
        # NewRange = (NewMax - NewMin)
        # NewValue = (((OldValue - OldMin) * NewRange) / OldRange) + NewMin

        oldXRange = (float((screen_size[0] + translateX) * scaleFactor) - translateX)
        newXRange = 3.5

        oldYRange = (float((screen_size[1] + translateY) * scaleFactor) + 1.0)
        newYRange = 2

        x0 = (((Px - translateX) * newXRange) / oldXRange) -2.5
        y0 = -1 * ((((Py - translateY) * newYRange) / oldYRange) - 1.0)
        x = 0.0
        y = 0.0
        iteration = 0

        while(x*x + y*y < 2 * 2 and iteration < self.max_iterations):
            xtemp = x*x - y*y + x0
            y = 2*x*y + y0
            x = xtemp
            iteration = iteration + 1

        self.stored_mandelbrot_calculations.append(iteration)
        return iteration

    def choose_color(self, iteration, base_color):
        number_of_shades = self.max_iterations
        current_shade = iteration

        if iteration == self.max_iterations:
            return 255, 255, 255

        shadow_blend_color = 255 * current_shade / number_of_shades
        r = (shadow_blend_color + base_color[0])/2
        g = (shadow_blend_color + base_color[1])/2
        b = (shadow_blend_color + base_color[2])/2

        return int(r), int(g), int(b)

    def block_draw(self, pygame, surface, screen_size, color, translateX, translateY, scale, blocks_remaining = []):
        self.max_iterations = 22 * scale
        block_division_factor = 5
        section_width = screen_size[0] / block_division_factor
        section_height = screen_size[1] / block_division_factor
        total_blocks = block_division_factor * block_division_factor

        #Populate an array containing every available block on the screen
        if len(blocks_remaining) == 0:
            for block in range(0, total_blocks):
                blocks_remaining.append(block)

        #Find a random block to fill in
        block_to_fill = random.choice(blocks_remaining)
        row_to_fill = block_to_fill / block_division_factor
        column_to_fill = block_to_fill % block_division_factor

        #Remove this block from the available block list
        blocks_remaining.remove(block_to_fill)

        #Draw a drop shadow - kinda neat!
        shadow_width = 25

        #The right side drop shadow
        drop_shadow_x_start = column_to_fill * section_width + section_width
        drop_shadow_x_end = drop_shadow_x_start + shadow_width
        do_not_draw = 0

        if column_to_fill == block_division_factor - 1 or block_to_fill + 1 not in blocks_remaining:
            do_not_draw = 1

        drop_shadow_y_start = row_to_fill * section_height + shadow_width
        drop_shadow_y_end = drop_shadow_y_start + section_height - shadow_width

        if do_not_draw == 0:
            for x in range(drop_shadow_x_start, drop_shadow_x_end):
                for y in range(drop_shadow_y_start, drop_shadow_y_end):
                    background_color = tuple(surface.get_at((x, y)))
                    r = (color[0] + background_color[0])/2
                    g = (color[1] + background_color[1])/2
                    b = (color[2] + background_color[2])/2

                    surface.set_at((x, y), (r,g,b))

        #The lower right corner drop shadow
        drop_shadow_x_start = column_to_fill * section_width + section_width
        drop_shadow_x_end = drop_shadow_x_start + shadow_width
        do_not_draw = 0

        if column_to_fill == block_division_factor - 1 or block_to_fill + block_division_factor + 1 not in blocks_remaining:
            do_not_draw = 1

        drop_shadow_y_start = row_to_fill * section_height + section_height
        drop_shadow_y_end = drop_shadow_y_start + shadow_width

        if do_not_draw == 0:
            for x in range(drop_shadow_x_start, drop_shadow_x_end):
                for y in range(drop_shadow_y_start, drop_shadow_y_end):
                    background_color = tuple(surface.get_at((x, y)))
                    r = (color[0] + background_color[0])/2
                    g = (color[1] + background_color[1])/2
                    b = (color[2] + background_color[2])/2

                    surface.set_at((x, y), (r,g,b))

        #The bottom drop shadow
        drop_shadow_x_start = column_to_fill * section_width + shadow_width
        drop_shadow_x_end = drop_shadow_x_start + section_width - shadow_width
        do_not_draw = 0

        if row_to_fill == block_division_factor - 1 or block_to_fill + block_division_factor not in blocks_remaining:
            do_not_draw = 1

        drop_shadow_y_start = row_to_fill * section_height + section_height
        drop_shadow_y_end = drop_shadow_y_start + shadow_width

        if do_not_draw == 0:
            for x in range(drop_shadow_x_start, drop_shadow_x_end):
                for y in range(drop_shadow_y_start, drop_shadow_y_end):
                    background_color = tuple(surface.get_at((x, y)))
                    r = (color[0] + background_color[0])/2
                    g = (color[1] + background_color[1])/2
                    b = (color[2] + background_color[2])/2

                    surface.set_at((x, y), (r,g,b))

        #Actually draw the block
        for x in range(column_to_fill * section_width, column_to_fill * section_width + section_width):
            for y in range(row_to_fill * section_height, row_to_fill * section_height + section_height):
                surface.set_at((x,y), self.choose_color(self.calculate_new_mandelbrot(screen_size, x, y, translateX, translateY, scale), color))
        pygame.display.flip()
        return blocks_remaining